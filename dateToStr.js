/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * jsdate-to-string
 * @see {@link https://framagit.org/tla/jsdate-to-string}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/jsdate-to-string/blob/master/LICENSE}
 *
 * @method dateToStr
 * @argument {Date|string|number} date
 * @argument {string} format
 *    #ampm : AM / PM
 *    #d : le numéro du jour dans le mois sur 1 ou 2 chiffres
 *    #dd : le numéro du jour dans le mois sur 2 chiffres
 *    #ddd : le nom du jour en chaîne courte
 *    #dddd : le nom du jour en chaîne complète
 *    #m : le numéro du mois sur 1 ou 2 chiffres
 *    #mm : le numéro du mois sur 2 chiffres
 *    #mmm : le nom du mois en chaîne courte
 *    #mmmm : le nom du mois en chaîne complète
 *    #yy : l'année sur 2 chiffres
 *    #yyyy : l'année sur 4 chiffres
 *    #12hh : l'heure en mode am-pm sur 2 chiffres
 *    #12h : l'heure en mode am-pm sur 1 ou 2 chiffres
 *    #h : l'heure sur 1 ou 2 chiffres
 *    #hh : l'heure sur 2 chiffres
 *    #n : les minutes sur 1 ou 2 chiffres
 *    #nn : les minutes sur 2 chiffres
 *    #s : les secondes sur 1 ou 2 chiffres
 *    #ss : les secondes sur 2 chiffres
 *    #z : les millisecondes sur 1 chiffre
 *    #zz : les millisecondes sur 2 chiffres
 *    #zzz : les millisecondes sur 3 chiffres
 * @argument {boolean} [UTC = false]
 * @return {string}
 * */
var dateToStr = (function () {
    var dt , _utc = false , obj = {
        'ampm' : function () {
            return _get( 'Hours' ) >= 12 ? 'PM' : 'AM';
        } ,
        'yyyy' : function () {
            return _get( 'FullYear' );
        } ,
        'yy' : function () {
            return (_get( 'FullYear' ) + '').slice( 2 );
        } ,
        'mmmm' : function () {
            return longMonth[ _get( 'Month' ) ];
        } ,
        'mmm' : function () {
            return shortMonth[ _get( 'Month' ) ];
        } ,
        'mm' : function () {
            return round( _get( 'Month' ) + 1 );
        } ,
        'm' : function () {
            return _get( 'Month' ) + 1;
        } ,
        'dddd' : function () {
            return longDay[ _get( 'Day' ) ];
        } ,
        'ddd' : function () {
            return shortDay[ _get( 'Day' ) ];
        } ,
        'dd' : function () {
            return round( _get( 'Date' ) );
        } ,
        'd' : function () {
            return _get( 'Date' );
        } ,
        '12hh' : function () {
            var h = +_get( 'Hours' );
            return h >= 12 ? round( h0( h - 12 ) ) : round( h0( h ) );
        } ,
        '12h' : function () {
            var h = +_get( 'Hours' );
            return h >= 12 ? h0( h - 12 ) : h0( h );
        } ,
        'hh' : function () {
            return round( _get( 'Hours' ) );
        } ,
        'h' : function () {
            return _get( 'Hours' );
        } ,
        'nn' : function () {
            return round( _get( 'Minutes' ) );
        } ,
        'n' : function () {
            return _get( 'Minutes' );
        } ,
        'ss' : function () {
            return round( _get( 'Seconds' ) );
        } ,
        's' : function () {
            return _get( 'Seconds' );
        } ,
        'zzz' : function () {
            return round( _get( 'Milliseconds' ) , 3 );
        } ,
        'zz' : function () {
            return round( (_get( 'Milliseconds' ) + '').slice( 0 , 2 ) );
        } ,
        'z' : function () {
            return (_get( 'Milliseconds' ) + '').slice( 0 , 1 );
        }
    } , keys = Object.keys( obj );
    
    var shortDay = [ "dim." , "lun." , "mar." , "mer." , "jeu." , "ven." , "sam." ] ,
        longDay = [ "dimanche" , "lundi" , "mardi" , "mercredi" , "jeudi" , "vendredi" , "samedi" ] ,
        shortMonth = [ "janv." , "févr." , "mars" , "avr." , "mai" , "juin" , "juil." , "aoüt" , "sept." , "oct." , "nov." , "déc." ] ,
        longMonth = [
            "janvier" , "février" , "mars" , "avril" , "mai" , "juin" ,
            "juillet" , "aoüt" , "septembre" , "octobre" , "novembre" , "décembre"
        ];
    
    function _get ( key ) {
        return dt[ 'get' + (_utc ? 'UTC' : '') + key ]();
    }
    
    function h0 ( n ) {
        return n == 0 ? 12 : n;
    }
    
    function round ( n , b ) {
        b = b || 2, n = n + '';
        while ( n.length < b ) n = '0' + n;
        return n;
    }
    
    function toX ( n ) {
        var r = '' , i = 0;
        while ( i++ < n ) r += 'x';
        return r;
    }
    
    return function ( date , format , UTC ) {
        var r , f , g , d , l , s , i;
        
        if ( !(date instanceof Date) && !(date = new Date( date ), date).getTime() && !(date = new Date( Date.parse( date ) ), date).getTime() ) {
            throw "dateToString need a valid Date instance / Date string or Date timestamp";
        }
        
        dt = date , _utc = !!UTC , r = format;
        
        keys.forEach( function ( k ) {
            g = new RegExp( '(#' + k + ')' , 'g' );
            while ( d = g.exec( format ) ) {
                s = d[ 0 ].trim() , l = s.length , i = d.index;
                if ( !obj[ (s = s.slice( 1 ) , s) ] ) continue;
                f = obj[ s ]() + '';
                r = r.slice( 0 , i ) + f + r.slice( i + l );
                format = format.slice( 0 , i ) + toX( f.length ) + format.slice( i + l );
            }
        } );
        
        return r;
    };
})();